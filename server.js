const express = require('express')
const cors = require('cors')
const path = require('path')
const session = require('express-session')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const config = require('./config/database-config')
const routes = require('./routes/routes')

mongoose.Promise = require('bluebird')
mongoose.connect(config.DB, { promiseLibrary: require('bluebird') })
    .then(() => console.log('Mongo connection successful'))
    .catch((err) => console.log(err));
      
const app = express()
app.use(bodyParser.json())
app.use(cors())
app.use(express.static('dist'))
app.use('/api', routes)
var port = process.env.PORT || 3000

app.listen(port, function(){
  console.log('listening on port....', port)
})