import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import User from './components/User.vue'
import Home from './components/Home.vue'
import Login from './components/Login.vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(BootstrapVue)
Vue.use(VueAxios, axios)
Vue.use(VueRouter)

const routes = [
  { name: 'User', path: '/create-user', component: User},
  { name: 'Login', path: '/login', component: Login},
  { name: 'Home', path: '/', component: Home}
]
const router = new VueRouter({ mode: 'history', routes: routes })

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  router
}).$mount('#app')
