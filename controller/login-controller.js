const User = require('../models/user')

exports.index = function (req, res, next) {
  User.findOne({email: req.body.email, password: req.body.password}, (err, user) => {
    if(err) {
      res.send({error: err});
      return next(err);
    } 
    if(!user) { res.send({error:'No user found'})}
    else {
        res.send({user})
    }
  })
}
