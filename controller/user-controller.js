const session = require('express-session')
const User = require('../models/user')

exports.createUser = (req, res, next) => {
  User.create(req.body, (err, user) => {
    if(err) return next(err);
    req.session.user = user
    res.redirect('/message')
  })
}
