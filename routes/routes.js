const express = require('express')
const router = express.Router()

const userController = require('../controller/user-controller')
const loginController = require('../controller/login-controller')

router.post('/create-user', userController.createUser)
router.post('/login', loginController.index)

module.exports = router
