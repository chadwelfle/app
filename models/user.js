const mongoose = require('mongoose');
const Schema = mongoose.Schema;


let UserSchema = new Schema({
    firstName: {type: String, required: true, maxlength: 100},
    lastName: {type: String, required: true, maxlength: 100},
    email: {type: String, required: true, match:  [/\S+@\S+\.\S+/, 'is invalid'], index: true, unique: true },
    password: {type: String, required: true, minlength: 6}
});

module.exports = mongoose.model('User', UserSchema);